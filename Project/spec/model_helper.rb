require 'unit_helper'

# ActiveRecord
require 'active_record'

spec = YAML.load(ERB.new(File.read('config/database.yml')).result)
ActiveRecord::Base.establish_connection(spec['test'])

# Squeel
require 'squeel'

# Shoulda Matchers
require 'shoulda-matchers'
require 'spec/support/delegate_matcher'
require 'spec/support/validate_duplication_of_matcher'

# For mocking models
require 'rspec/rails/mocks'

# EnumerateIt
require 'enumerate_it'

require 'protected_attributes'

class ActiveRecord::Base
  extend EnumerateIt
end

Dir['app/enumerations/*.rb'].each do |file|
  require File.expand_path(file)
end

Dir['app/enumerations/payments_form/*.rb'].each do |file|
  require File.expand_path(file)
end

# AwesomeNestedNet
require 'awesome_nested_set'

# ActiveRecord::Attributes
require 'lib/active_record/list_attributes'
require 'lib/active_record/search_attributes'
require 'lib/active_record/modal_attributes'
require 'lib/active_record/orderize'
require 'lib/active_record/filterize'
class ActiveRecord::Base
  include ActiveRecord::ListAttributes
  include ActiveRecord::SearchAttributes
  include ActiveRecord::ModalAttributes
  include ActiveRecord::Orderize
  include ActiveRecord::Filterize
end

# Paperclip
require 'paperclip'
require 'paperclip/matchers'

RSpec.configure do |config|
  config.include Paperclip::Shoulda::Matchers
end

class ActiveRecord::Base
  include Paperclip::Glue
end

# Devise
require 'devise'
require 'devise/orm/active_record'

# Brazilian-rails
require 'brcpfcnpj'
require 'brdinheiro'
require 'brstring'

# I18n
require 'i18n'

I18n.load_path += Dir['config/locales/**/*.yml']
I18n.default_locale = 'pt-BR'

# I18n::Alchemy
require 'i18n_alchemy'

class ActiveRecord::Base
  include I18n::Alchemy
end

require 'config/initializers/inflections'

# Custom matchers
RSpec::Matchers.define :have_error_on do |field, options|
  match do |model|
    message = options ? options[:message] : nil

    if message
      expect(model.errors[field]).to be_include message
    else
      expect(model.errors[field]).to_not be_empty
    end
  end

  failure_message_for_should do |model|
    message = options.fetch(:message, '')

    "expected #{model.inspect} to have an error #{message.inspect} on field #{field.inspect}"
  end

  failure_message_for_should_not do |model|
    message = options.fetch(:message, '')

    "expected #{model.inspect} to have an error #{message.inspect} on field #{field.inspect}"
  end
end
