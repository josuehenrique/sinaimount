require 'model_helper'
require 'app/models/post'

describe Job do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:classification) }

  describe '#to_s' do
    it 'return name of Post' do
      subject.name = 'Líder de louvor'
      expect(subject.to_s).to eq 'Líder de louvor'
    end
  end
end
