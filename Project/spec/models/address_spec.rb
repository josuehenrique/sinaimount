require 'model_helper'
require 'app/models/address'

describe Address do
  it { should belong_to(:city) }
  it { should belong_to(:related) }

  it { should validate_presence_of(:city_id) }
  it { should validate_presence_of(:street) }
  it { should validate_presence_of(:number) }
  it { should validate_presence_of(:district) }
  it { should validate_presence_of(:zipcode) }

  it { should ensure_length_of(:zipcode).is_equal_to(8) }

  describe '#to_s' do
    it 'return street of address' do
      subject.street = 'RUA INGLATERRA'
      expect(subject.to_s).to eq 'RUA INGLATERRA'
    end
  end
end
