require 'model_helper'
require 'app/models/city'
require 'app/models/state'

describe State do
  it { should belong_to(:country) }
  it { should have_many(:cities) }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:acronym) }

  describe '#to_s' do
    it 'return name of state with acronym' do
      subject.name = 'GOIÁS'
      subject.acronym = 'GO'
      expect(subject.to_s).to eq 'GOIÁS - GO'
    end
  end
end
