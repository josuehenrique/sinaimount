require 'model_helper'
require 'app/models/congregated'

describe Congregated do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:birth_dt) }
  it { should validate_presence_of(:entry_dt) }

  describe '#to_s' do
    it 'return name of Congregated' do
      subject.name = 'MARCOS CARVALHO'
      expect(subject.to_s).to eq 'MARCOS CARVALHO'
    end
  end
end
