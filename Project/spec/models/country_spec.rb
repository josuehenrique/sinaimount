require 'model_helper'
require 'app/models/country'

describe Country do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:acronym) }
end
