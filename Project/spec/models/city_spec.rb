require 'model_helper'
require 'app/models/city'

describe City do
  it { should belong_to(:state) }

  it { should validate_presence_of(:name) }
end
