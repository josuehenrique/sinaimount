require 'model_helper'
require 'app/models/address'
require 'app/models/church'

describe Church do
  it { should have_one(:address) }

  it { should validate_presence_of(:name) }

  describe '#to_s' do
    it 'return name of church' do
      subject.name = 'CRISTÃ EVANGÉLICA DA BOA VISTA'
      expect(subject.to_s).to eq 'CRISTÃ EVANGÉLICA DA BOA VISTA'
    end
  end
end
