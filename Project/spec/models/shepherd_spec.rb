require 'model_helper'
require 'app/models/address'
require 'app/models/church'
require 'app/models/shepherd'

describe Shepherd do
  it { should have_one(:address) }
  it { should belong_to(:church) }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:birth_dt) }
  it { should validate_presence_of(:church_id) }

  describe '#to_s' do
    it 'return name of Shepherd' do
      subject.name = 'MARCOS CARVALHO'
      expect(subject.to_s).to eq 'MARCOS CARVALHO'
    end
  end
end
