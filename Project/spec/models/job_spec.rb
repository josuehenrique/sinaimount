require 'model_helper'
require 'app/models/job'

describe Job do
  it { should validate_presence_of(:name) }

  describe '#to_s' do
    it 'return name of job' do
      subject.name = 'ENGENHEIRO'
      expect(subject.to_s).to eq 'ENGENHEIRO'
    end
  end
end
