FactoryGirl.define do
  factory :city, aliases: [:city_ANAPOLIS]  do
    name 'Anápolis'
    association :state, factory: :state_GOIAS
    active true
  end
end
