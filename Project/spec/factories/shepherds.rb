FactoryGirl.define do
  factory :shepherd, aliases: [:shepherd_MARCOS] do
    name 'MARCOS CARVALHO'
    association :address, factory: :address_MARCOS
    association :church, factory: :church_ICEB
    birth_dt Date.new('15-01-1965')
    active true
  end
end
