FactoryGirl.define do
  factory :state, aliases: [:state_GOIAS] do
    name 'Goiás'
    acronym 'GO'
    association :country, factory: :country_BRASIL
    active true
  end
end
