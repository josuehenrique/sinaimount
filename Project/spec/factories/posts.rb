FactoryGirl.define do
  factory :jobs, aliases: [:post_WORSHIPLEADER] do
    name 'Lider de Louvor'
    classification PostsType::LEADERSHIP
    active true
  end
end
