FactoryGirl.define do
  factory :churches, aliases: [:church_ICEB] do
    name 'CRISTÃ EVANGÉLICA DA BOA VISTA'
    association :address, factory: :address_ICEB
    active true
  end
end
