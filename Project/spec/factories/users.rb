FactoryGirl.define do
  factory :user do
    email 'user@hotmail.com.br'
    password '12345678'
    login 'sinaimount'
    administrator false
    initialize_with { User.find_or_create_by(email: email) }

    factory :admin do
      login 'admin'
      administrator true
    end

    factory :jack do
      login 'jack'
      email 'jack@hotmail.com.br'
    end
  end
end
