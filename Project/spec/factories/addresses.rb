FactoryGirl.define do
  factory :address, aliases: [:address_ICEB] do
    association :city, factory: :city_ANAPOLIS
    street 'RUA INGLATERRA'
    complement 'QD.: 07 LT.: 11'
    number 0
    district 'BOA VISTA'
    zipcode '75083030'
    lat -16.300642
    long -48.943689
    refernce_point 'Hospital Espírita'
    active true

    factory :address_MARCOS do
      street 'RUA S. RAMOS'
      complement 'QD.: 07 LT.: 09'
      number 0
      district 'CIDADE UNIVERSITÁRIA'
      zipcode '75070485'
      lat -16.296481
      long -48.9407
      refernce_point 'UNIEVANGÉLICA'
    end
  end
end