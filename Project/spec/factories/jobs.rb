FactoryGirl.define do
  factory :jobs, aliases: [:job_ENGINEER] do
    name 'ENGENHEIRO'
    active true
  end
end
