FactoryGirl.define do
  factory :country, aliases: [:country_BRASIL] do
    name 'Brasil'
    acronym 'BR'
    active true
  end
end
