FactoryGirl.define do
  factory :congregated, aliases: [:congregated_JOAO] do
    name 'JOÃO TRINDADE E SOUZA'
    birth_dt Date.new(1965,01,15)
    entry_dt Date.new(2000,01,15)
    baptized true
    active true
  end
end
