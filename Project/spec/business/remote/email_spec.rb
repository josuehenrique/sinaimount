require 'model_helper'
require 'app/business/remote/email'

describe Remote::Email do
  let(:settings) { double(remote_connection: true) }
  let(:server_repository) { double }
  let(:configuration_repository) { double(email_uid: 10, email_gid: 15) }
  let(:remote_email_account_repository) { double }
  let(:remote_alias_class) { double }
  let(:ssh_class) { double }
  let(:email_alias) { double }
  let(:aliases) { [email_alias] }
  let(:server) { double(:server, ip: '10.0.0.1', usuario: 'user', password: 'pass') }
  let(:dominio) { double(:dominio, id: 100, nome: 'email.com') }
  let(:email) { double(:email, dominio: dominio, usuario: 'full',
                     full: 'full@email.com', senha: 'pass', quota: 100) }

  subject do
    described_class.new(email, settings, server_repository)
  end

  context 'Not configured to remote connect' do
    it 'should raise an error' do
      settings.stub(:remote_connection).and_return(false)

      expect { subject.create }.to raise_error(RuntimeError, 'Ambiente configurado para NÃO utilizar conexão remota.')
    end
  end

  it 'should create an email at remote server' do
    server_repository.should_receive(:email).with(100).and_return(server)

    subject.stub(:connect_email_account)
    subject.stub(:remote_email_account_repository).and_return(remote_email_account_repository)
    subject.stub(:configuration_repository).and_return(configuration_repository)

    remote_email_account_repository.should_receive(:new).and_return(remote_email_account_repository)
    remote_email_account_repository.should_receive(:usuario=).with('full@email.com')
    remote_email_account_repository.should_receive(:senha=)
    remote_email_account_repository.should_receive(:dominio=).with('email.com')
    remote_email_account_repository.should_receive(:home=).with('/email/email.com/full/')
    remote_email_account_repository.should_receive(:maildir=).with('Maildir')
    remote_email_account_repository.should_receive(:quota=).with(100)
    remote_email_account_repository.should_receive(:ativo=).with(1)
    remote_email_account_repository.should_receive(:uid=).with(10)
    remote_email_account_repository.should_receive(:gid=).with(15)
    remote_email_account_repository.should_receive(:save!)

    subject.stub(:remote_alias_class).and_return(remote_alias_class)

    remote_alias_class.should_receive(:create).with('full@email.com', 'full@email.com')

    subject.stub(:ssh_class).and_return(ssh_class)

    ssh_class.should_receive(:start).with("10.0.0.1", "user", {password: "pass"})

    email.should_receive(:encrypt_password)

    subject.create
  end

  it 'should update an email at remote server' do
    server_repository.should_receive(:email).with(100).and_return(server)

    subject.stub(:connect_email_account)
    subject.stub(:find_remote_account).and_return(remote_email_account_repository)

    remote_email_account_repository.should_receive(:senha=)
    remote_email_account_repository.should_receive(:quota=).with(100)
    remote_email_account_repository.should_receive(:save!)

    subject.update
  end

  it 'should change the email situation to active' do
    server_repository.should_receive(:email).with(100).and_return(server)

    subject.stub(:connect_email_account)
    subject.stub(:find_remote_account).and_return(remote_email_account_repository)

    remote_email_account_repository.should_receive(:ativo=).with(1)
    remote_email_account_repository.should_receive(:save!)

    subject.change_situation(1)
  end

  it 'should change the email situation to inactive' do
    server_repository.should_receive(:email).with(100).and_return(server)

    subject.stub(:connect_email_account)
    subject.stub(:find_remote_account).and_return(remote_email_account_repository)

    remote_email_account_repository.should_receive(:ativo=).with(0)
    remote_email_account_repository.should_receive(:save!)

    subject.change_situation(0)
  end

  it 'should destroy the email' do
    server_repository.should_receive(:email).with(100).and_return(server)

    subject.stub(:connect_email_account)
    subject.stub(:connect_email_alias)
    subject.stub(:find_remote_account).and_return(remote_email_account_repository)
    subject.stub(:ssh_class).and_return(ssh_class)

    ssh_class.should_receive(:start).with("10.0.0.1", "user", {password: "pass"})

    remote_email_account_repository.should_receive(:destroy)
    remote_email_account_repository.should_receive(:aliases).and_return(aliases)
    email_alias.should_receive(:destroy)

    subject.destroy
  end
end
