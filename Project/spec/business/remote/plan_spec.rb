require 'model_helper'
require 'app/business/remote/plan'

describe Remote::Plan do
  let(:settings) { double(remote_connection: true) }
  let(:plan) { double(:plan, id: 5) }
  let(:group_check_repository) { double(:group_check_repository) }
  let(:group_reply_repository) { double(:group_reply_repository) }
  let(:group_reply) { double(:group_reply) }

  subject do
    described_class.new(plan, settings)
  end

  context 'Not configured to remote connect' do
    it 'should raise an error' do
      settings.stub(:remote_connection).and_return(false)

      expect { subject.create }.to raise_error(RuntimeError, 'Ambiente configurado para NÃO utilizar conexão remota.')
    end
  end

  it 'should create a plan at remote server' do
    plan.should_receive(:radius_server)

    subject.stub(:connect_group_check).and_return(group_check_repository)
    subject.stub(:connect_group_reply).and_return(group_reply_repository)

    group_check_repository.should_receive(:new).and_return(group_check_repository)
    group_check_repository.should_receive(:groupname=).with("pr5")
    group_check_repository.should_receive(:[]=).with(:attribute, "Auth-Type")
    group_check_repository.should_receive(:op=).with(":=")
    group_check_repository.should_receive(:value=).with("Local")
    group_check_repository.should_receive(:save!)

    group_check_repository.should_receive(:new).and_return(group_check_repository)
    group_check_repository.should_receive(:groupname=).with("pr5")
    group_check_repository.should_receive(:[]=).with(:attribute, "Simultaneous-Use")
    group_check_repository.should_receive(:op=).with(":=")
    group_check_repository.should_receive(:value=).with("1")
    group_check_repository.should_receive(:save!)

    plan.should_receive(:speed).with(true).and_return('500k/1000k')

    group_reply_repository.should_receive(:new).and_return(group_reply_repository)
    group_reply_repository.should_receive(:groupname=).with("pr5")
    group_reply_repository.should_receive(:[]=).with(:attribute, "Mikrotik-Rate-Limit")
    group_reply_repository.should_receive(:op=).with(":=")
    group_reply_repository.should_receive(:value=).with("500k/1000k")
    group_reply_repository.should_receive(:save!)

    subject.create
  end

  it 'should update plan speed' do
    plan.should_receive(:radius_server)

    subject.stub(:connect_group_reply).and_return(group_reply_repository)

    group_reply_repository.should_receive(:find_by_attribute_and_groupname).with("Mikrotik-Rate-Limit", "pr5").and_return(group_reply)

    plan.should_receive(:speed).with(true).and_return('500k/1000k')

    group_reply.should_receive(:value=).with("500k/1000k")
    group_reply.should_receive(:save!)

    subject.update_speed!
  end
end
