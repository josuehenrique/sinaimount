require 'unit_helper'
require 'app/business/remote/connect_server'

describe Remote::ConnectServer do
  let(:repository) { double(:repository) }
  let(:server) { double(:server, ip: '10.0.0.1', usuario: 'user', password: 'pass') }

  context 'Alias' do
    subject do
      described_class.new(:alias, server)
    end

    it 'should connect to alias table' do
      subject.stub(:remote_alias_repository).and_return(repository)
      repository.should_receive(:establish_connection).with(
        {adapter: "mysql2", host: "10.0.0.1", database: "email", username: "user", password: "pass"}
      )

      subject.email
    end
  end

  context 'Email account' do
    subject do
      described_class.new(:email_account, server)
    end

    it 'should connect to alias table' do
      subject.stub(:remote_email_account_repository).and_return(repository)
      repository.should_receive(:establish_connection).with(
        {adapter: "mysql2", host: "10.0.0.1", database: "email", username: "user", password: "pass"}
      )

      subject.email
    end
  end

  context 'Group check' do
    subject do
      described_class.new(:group_check, server)
    end

    it 'should connect to alias table' do
      subject.stub(:remote_group_check_repository).and_return(repository)
      repository.should_receive(:establish_connection).with(
        {adapter: "mysql2", host: "10.0.0.1", database: "radius", username: "user", password: "pass"}
      )

      subject.radius
    end
  end

  context 'Group reply' do
    subject do
      described_class.new(:group_reply, server)
    end

    it 'should connect to alias table' do
      subject.stub(:remote_group_reply_repository).and_return(repository)
      repository.should_receive(:establish_connection).with(
        {adapter: "mysql2", host: "10.0.0.1", database: "radius", username: "user", password: "pass"}
      )

      subject.radius
    end
  end
end
