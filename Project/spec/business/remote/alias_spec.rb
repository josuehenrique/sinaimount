require 'model_helper'
require 'app/business/remote/alias'
require 'app/models/remote_alias'

describe Remote::Alias do
  let(:settings) { double(remote_connection: true) }
  let(:dominio_repository) { double }
  let(:servidor_repository) { double }
  let(:dominio) { double(id: 1, nome: 'email.com') }
  let(:remote_alias_repository) { double }
  let(:email_alias) { double }
  let(:aliases) { [email_alias] }

  subject do
    described_class.new('source@email.com', 'destiny@email.com', settings, dominio_repository, servidor_repository)
  end

  context 'Not configured to remote connect' do
    it 'should raise an error' do
      settings.stub(:remote_connection).and_return(false)

      expect { subject.create }.to raise_error(RuntimeError, 'Ambiente configurado para NÃO utilizar conexão remota.')
    end
  end

  it 'should create an alias at remote server' do
    dominio_repository.should_receive(:find_by_nome).with('email.com').and_return(dominio)

    servidor_repository.should_receive(:email).with(1)

    subject.stub(:connect_email_alias)
    subject.stub(:remote_alias_repository).and_return(remote_alias_repository)

    remote_alias_repository.should_receive(:new).and_return(remote_alias_repository)
    remote_alias_repository.should_receive(:origem=).with('source@email.com')
    remote_alias_repository.should_receive(:destino=).with('destiny@email.com')
    remote_alias_repository.should_receive(:dominio=).with('email.com')
    remote_alias_repository.should_receive(:save!)

    subject.create
  end

  it 'should destroy an alias at remote server' do
    dominio_repository.should_receive(:find_by_nome).with('email.com').and_return(dominio)

    servidor_repository.should_receive(:email).with(1)

    subject.stub(:connect_email_alias)
    subject.stub(:remote_alias_repository).and_return(remote_alias_repository)

    remote_alias_repository.should_receive(:where).
      with({origem: "source@email.com", destino: "destiny@email.com"}).and_return(aliases)
    email_alias.should_receive(:destroy)

    subject.destroy
  end
end
