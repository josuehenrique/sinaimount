require 'model_helper'
require 'app/business/role_definition'
require 'app/models/permit'

describe RoleDefinition do
  let :customers_role do
    double('customers_role', controller: 'customers')
  end

  let :contacts_role do
    double('contacts_role', controller: 'contacts')
  end

  let :object do
    double('object')
  end

  let :permission do
    double('permission')
  end

  let :i18n do
    double('I18n', translate: { customers: "Customers" })
  end

  it 'do not duplicate existent roles' do
    object.stub(:roles).and_return([customers_role])

    object.should_receive(:build_role).never

    object_updater = RoleDefinition.new(object, nil, i18n)
    object_updater.stub(:current_permits).and_return([contacts_role])
    object_updater.stub(:available_permits).and_return([contacts_role])
    object_updater.stub(:left_roles).and_return([])
    object_updater.update
  end

  it 'should create inexistent roles' do
    object.stub(:roles).and_return([])

    object.should_receive(:build_role).with({permit: contacts_role})

    object_updater = RoleDefinition.new(object, nil, i18n)
    object_updater.stub(:roles_from_permits).and_return([])
    object_updater.stub(:available_permits).and_return([contacts_role])
    object_updater.update
  end

  it 'should destroy left roles' do
    object.stub(:roles).and_return([customers_role, contacts_role])

    object.should_receive(:delete_role)
    object.should_receive(:delete_role)

    object_updater = RoleDefinition.new(object, nil, i18n)
    object_updater.stub(:roles_from_permits).and_return([customers_role, contacts_role])
    object_updater.stub(:available_permits).and_return([customers_role, contacts_role])
    object_updater.stub(:current_permits).and_return([customers_role, contacts_role])
    object_updater.update
  end
end
