require 'unit_helper'
require 'model_helper'
require 'app/models/cidade'
require 'app/models/un'
require 'app/models/relacoesun'
require 'app/business/unit_relations'

describe UnitRelations do
  let(:un) { double(:un, id: 1) }
  let(:un_repository) { Un }
  let(:selected_objects) { [1, 2, 3, 4, 5] }
  let(:repository) { Relacoesun }
  let(:relacaoun) { double(:relacaoun) }

  context '#cities' do
    let(:city_repository) { Cidade }
    let(:selected_cities) { [city1, city2] }
    let(:city1) { double(:city1, id: 1) }
    let(:city2) { double(:city2, id: 2) }
    let(:city3) { double(:city3, id: 3) }

    subject { described_class.new(un.id, selected_objects, city_repository) }

    it 'update the cities of business unit' do
      # Find the business unit
      un_repository.should_receive(:find).with(1).and_return(un)

      # Find the selected cities
      city_repository.should_receive(:find).with([1, 2, 3, 4, 5]).and_return(selected_cities)

      # Cities linked to business unit
      un.should_receive(:cidades).and_return([city3])

      # Create 2 new cities that didn't exist
      repository.stub(:create).with(un: un, relacionado: city1)
      repository.stub(:create).with(un: un, relacionado: city2)

      # Set available cities
      subject.stub(:available).and_return([city1, city2, city3])

      # Should remove 1 city, that is not selected
      subject.stub(:links_to_delete).and_return([relacaoun])
      relacaoun.stub(:destroy)

      subject.update
    end
  end
end
