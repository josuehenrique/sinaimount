require 'model_helper'
require 'app/business/generate_default_configuration'
require 'app/models/configuracao'

describe GenerateDefaultConfiguration do
  subject { described_class.new }

  it 'generate the missing configuration with default values' do
    subject.should_receive(:not_created?).with(:token)

    subject.do!
  end
end
