require 'unit_helper'
require 'model_helper'
require 'app/models/funcionario'
require 'app/models/un'
require 'app/models/relacoesfuncionario'
require 'app/business/employee_relations'

describe EmployeeRelations do
  let(:funcionario) { double(:funcionario, id: 1) }
  let(:employee_repository) { Funcionario }
  let(:selected_objects) { [1, 2, 3, 4, 5] }
  let(:repository) { Relacoesfuncionario }
  let(:relacaofuncionario) { double(:relacaofuncionario) }

  context '#uns' do
    let(:un_repository) { Un }
    let(:selected_uns) { [un1, un2] }
    let(:un1) { double(:un1, id: 1) }
    let(:un2) { double(:un2, id: 2) }
    let(:un3) { double(:un3, id: 3) }

    subject { described_class.new(funcionario.id, selected_objects, un_repository) }

    it 'update the uns of employee' do
      # Find the employee
      employee_repository.should_receive(:find).with(1).and_return(funcionario)

      # Find the selected business units
      un_repository.should_receive(:find).with([1, 2, 3, 4, 5]).and_return(selected_uns)

      # Business units linked to employee
      funcionario.should_receive(:uns).and_return([un3])

      # Create 2 new business unit that didn't exist
      repository.stub(:create).with(funcionario: funcionario, relacionado: un1)
      repository.stub(:create).with(funcionario: funcionario, relacionado: un2)

      # Set available business units
      subject.stub(:available).and_return([un1, un2, un3])

      # Should remove 1 un, that is not selected
      subject.stub(:links_to_delete).and_return([relacaofuncionario])
      relacaofuncionario.stub(:destroy)

      subject.update
    end
  end

  context '#uns - single permission' do
    let(:un_repository) { Un }
    let(:un) { double(:un, id: 1) }

    subject { described_class.new(funcionario.id, un.id, un_repository) }

    it 'it should give permission at un' do
      employee_repository.should_receive(:find).with(1).and_return(funcionario)

      un_repository.should_receive(:find).with(1).and_return(un)

      repository.should_receive(:create).with(funcionario: funcionario, relacionado: un)

      subject.grant_single_permission
    end
  end
end
