require 'spec_helper'

feature 'Coutries' do
  background do
    sign_in
  end

  scenario 'create/update/activate/inactivate country' do
    navigate 'Administração > Países, Estados e Cidades'

    click_link 'Cadastrar'

    fill_in 'Nome', with: 'Brasil'
    fill_in 'Sigla', with: 'BR'

    click_button 'Salvar'

    expect(page).to have_notice 'País criado com sucesso.'

    click_link 'Editar'

    expect(page).to have_field 'Nome', with: 'Brasil'
    expect(page).to have_field 'Sigla', with: 'BR'

    fill_in 'Nome', with: 'Argentina'
    fill_in 'Sigla', with: 'AR'

    click_button 'Salvar'

    expect(page).to have_notice 'País editado com sucesso.'

    click_link 'Voltar'

    click_link 'Inativar', confirm: true

    expect(page).to have_notice 'Inativado com sucesso.'

    expect(page).to_not have_content 'Argentina'

    click_link 'Inativo'

    click_link 'Ativar', confirm: true

    expect(page).to have_notice 'Ativado com sucesso.'

    click_link 'Inativo'

    expect(page).to_not have_content 'Argentina'
  end
end
