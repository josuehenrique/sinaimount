require 'spec_helper'

feature 'Churches' do
  background do
    sign_in
  end

  scenario 'create/update/activate/inactivate church' do

    navigate 'Administração > Igrejas'

    click_link 'Cadastrar'

    create(:city_ANAPOLIS)

    fill_in 'Nome', with: 'Cristã Evangélica Boa Vista'
    fill_in 'CEP', with: '75083030'
    print
    select 'GO - Goiás', from: 'Estado'
    select 'Anápolis', from: 'Cidade'
    fill_in 'Bairro', with: 'BOA VISTA'
    fill_in 'Logradouro', with: 'RUA INGLATERRA'
    fill_in 'Complemento', with: 'QD.: 07 LT.: 11'
    fill_in 'Número', with: '00000'
    fill_in 'Ponto de Referência', with: 'Hospital Espírita'
    fill_in 'Latitude', with: -16.300642
    fill_in 'Longitude', with: -48.943689

    click_button 'Salvar'

    expect(page).to have_notice 'Igreja criada com sucesso.'

    click_link 'Editar'

    expect(page).to have_field 'Nome', with: 'Cristã Evangélica Boa Vista'
    expect(page).to have_field 'CEP', with: '75083030'
    expect(page).to have_select 'Estado', with: 'Goiás - GO'
    expect(page).to have_select 'Cidade', with: 'Anápolis'
    expect(page).to have_field 'Bairro', with: 'BOA VISTA'
    expect(page).to have_field 'Logradouro', with: 'Engenheiro(a)'
    expect(page).to have_field 'Complemento', with: 'Engenheiro(a)'
    expect(page).to have_field 'Número', with: '00000'
    expect(page).to have_field 'Ponto de Referência', with: 'Hospital Espírita'
    expect(page).to have_field 'Latitude', with: -16.300642
    expect(page).to have_field 'Longitude', with: -48.943689

    fill_in 'Nome', with: 'Cristã Evangélica Maracanã'

    click_button 'Salvar'

    expect(page).to have_notice 'Igreja editada com sucesso.'

    click_link 'Voltar'

    click_link 'Inativar', confirm: true

    expect(page).to have_notice 'Inativado com sucesso.'

    expect(page).to_not have_content 'Cristã Evangélica Maracanã'

    click_link 'Inativo'

    click_link 'Ativar', confirm: true

    expect(page).to have_notice 'Ativado com sucesso.'

    click_link 'Inativo'

    expect(page).to_not have_content 'Cristã Evangélica Maracanã'
  end
end
