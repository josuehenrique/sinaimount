require 'spec_helper'

feature 'States' do
  background do
    sign_in
  end

  scenario 'create/update/activate/inactivate state' do
    make_dependencies!

    navigate 'Administração > Países, Estados e Cidades'

    click_link 'Brasil'

    click_link 'Cadastrar'

    fill_in 'Nome', with: 'Goiás'
    fill_in 'Sigla', with: 'GO'

    click_button 'Salvar'

    expect(page).to have_notice 'Estado criado com sucesso.'

    click_link 'Editar'

    expect(page).to have_field 'Nome', with: 'Goiás'
    expect(page).to have_field 'Sigla', with: 'GO'

    fill_in 'Nome', with: 'Minas Gerais'
    fill_in 'Sigla', with: 'MG'

    click_button 'Salvar'

    expect(page).to have_notice 'Estado editado com sucesso.'

    click_link 'Voltar'

    click_link 'Inativar', confirm: true

    expect(page).to have_notice 'Inativado com sucesso.'

    expect(page).to_not have_content 'Minas Gerais'

    click_link 'Inativo'

    click_link 'Ativar', confirm: true

    expect(page).to have_notice 'Ativado com sucesso.'

    click_link 'Inativo'

    expect(page).to_not have_content 'Minas Gerais'
  end

  def make_dependencies!
    create(:country_BRASIL)
  end
end
