require 'spec_helper'

feature 'Jobs' do
  background do
    sign_in
  end

  scenario 'create/update/activate/inactivate job' do

    navigate 'Administração > Empregos'

    click_link 'Cadastrar'

    fill_in 'Nome', with: 'Engenheiro(a)'

    click_button 'Salvar'

    expect(page).to have_notice 'Emprego criado com sucesso.'

    click_link 'Editar'

    expect(page).to have_field 'Nome', with: 'Engenheiro(a)'

    fill_in 'Nome', with: 'Arquiteto(a)'

    click_button 'Salvar'

    expect(page).to have_notice 'Emprego editado com sucesso.'

    click_link 'Voltar'

    click_link 'Inativar', confirm: true

    expect(page).to have_notice 'Inativado com sucesso.'

    expect(page).to_not have_content 'Arquiteto(a)'

    click_link 'Inativo'

    click_link 'Ativar', confirm: true

    expect(page).to have_notice 'Ativado com sucesso.'

    click_link 'Inativo'

    expect(page).to_not have_content 'Arquiteto(a)'
  end
end
