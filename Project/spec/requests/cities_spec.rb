require 'spec_helper'

feature 'Cities' do
  background do
    sign_in
  end

  scenario 'create/update/activate/inactivate city' do
    make_dependencies!

    navigate 'Administração > Países, Estados e Cidades'

    click_link 'Brasil'

    click_link 'Goiás'

    click_link 'Cadastrar'

    fill_in 'Nome', with: 'Ceres'

    click_button 'Salvar'

    expect(page).to have_notice 'Cidade criada com sucesso.'

    click_link 'Editar'

    expect(page).to have_field 'Nome', with: 'Ceres'

    fill_in 'Nome', with: 'Abadiania'

    click_button 'Salvar'

    expect(page).to have_notice 'Cidade editada com sucesso.'

    click_link 'Voltar'

    click_link 'Inativar', confirm: true

    expect(page).to have_notice 'Inativado com sucesso.'

    expect(page).to_not have_content 'Abadiania'

    click_link 'Inativo'

    click_link 'Ativar', confirm: true

    expect(page).to have_notice 'Ativado com sucesso.'

    click_link 'Inativo'

    expect(page).to_not have_content 'Abadiania'
  end

  def make_dependencies!
      create(:state_GOIAS)
  end
end
