ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/autorun'
require 'paperclip/matchers'

Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

RSpec.configure do |config|
  config.treat_symbols_as_metadata_keys_with_true_values = true

  config.use_transactional_fixtures = false

  config.infer_base_class_for_anonymous_controllers = false

  config.mock_with :rspec

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  # filtering by tags
  if ENV['FILTER_TAGS']
    config.filter_run_excluding intermittent: true
  end

  config.include Paperclip::Shoulda::Matchers

  config.include FactoryGirl::Syntax::Methods

  config.before(:suite) do
    # Forces all threads to share the same connection. This works on
    # Capybara because it starts the web server in a thread.
    ActiveRecord::Base.shared_connection = ActiveRecord::Base.connection
  end
end

module ::RSpec::Core
  class ExampleGroup
    include Capybara::DSL
    include Capybara::RSpecMatchers
  end
end
