require 'model_helper'
require 'app/models/configuracao'
require 'lib/crypt'
require 'active_support/message_encryptor'
require 'active_support/message_verifier'

describe Crypt do
  let(:password) {'minhasupersenha'}

  it 'it should crypt/decrypt the password' do
    encrypted = Crypt.encrypt(password)

    expect(Crypt.decrypt encrypted).to eq password
  end
end
