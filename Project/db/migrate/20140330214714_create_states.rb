class CreateStates < ActiveRecord::Migration
  def change
    create_table :states do |t|
      t.string :name, limit: 60
      t.string :acronym, limit: 2
      t.references :country, index: true, null: false
      t.boolean :active, default: true

      t.timestamps
    end
    add_foreign_key 'states', 'country_id', 'countries'
  end
end