class InsertBrazilianCountryInCountries < ActiveRecord::Migration
  def change
    execute("
      INSERT INTO `countries`  (`id`, `name`, `acronym`, `active`,`created_at`,`updated_at`) VALUES
                    (1, 'Brasil', 'BR', 1,'#{Time.now.to_s(:db)}','#{Time.now.to_s(:db)}');
    ")
  end
end
