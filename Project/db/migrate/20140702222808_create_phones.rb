class CreatePhones < ActiveRecord::Migration
  def change
    create_table :phones do |t|
      t.string :ddi
      t.string :ddd
      t.string :number
      t.references :related, index: true,  polymorphic: true, null: false
      t.references :phone_carrier
      t.timestamps
    end
    add_foreign_key 'phones', 'phone_carrier_id', 'phone_carriers'
  end
end
