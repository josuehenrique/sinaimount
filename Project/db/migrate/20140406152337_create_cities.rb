class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :name, limit: 150, null: false
      t.references :state, index:true, null: false
      t.boolean :active, default: true

      t.timestamps
    end
    add_foreign_key 'cities', 'state_id', 'states'
  end
end
