class Ability
  include CanCan::Ability

  def initialize(user = nil)
    alias_action :modal, to: :read

    return unless user

    if user.administrator?
      can :access, :all
    else
      if user.roles.blank? && user.funcionario
        # If the user has no permissions, use from his post
        user.funcionario.cargo.roles.each do |role|
          can role.action.to_sym, role.controller.to_sym
        end
      else
        user.roles.each do |role|
          can role.action.to_sym, role.controller.to_sym
        end
      end
    end
  end
end
