class Crypt
  Key = Configuracao.token || 'token'

  def self.encrypt(string, code = Key)
    secret = Digest::SHA1.hexdigest(code)
    ActiveSupport::MessageEncryptor.new(secret).encrypt_and_sign(string).to_s
  end

  def self.decrypt(string, code = Key)
    secret = Digest::SHA1.hexdigest(code)
    begin
      ActiveSupport::MessageEncryptor.new(secret).decrypt_and_verify(string).to_s
    rescue
      # Remover após recriptografar todas senhas
      ActiveSupport::MessageEncryptor.new(secret).decrypt(string).to_s
    end
  end
end
