namespace :tree do
  desc "Generate codes for planocontas"
  task planoconta_codes: :environment do
    Planoconta.find_each do |planoconta|
      planoconta.set_code
    end
  end
end
