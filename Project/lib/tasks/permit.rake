namespace :permit do
  desc "Loading all models and their related controller methods inpermissions table."
  task(create: :environment) do
    arr = controllers

    arr.each do |controller|
      #only that controller which represents a model
      name = controller_name(controller)
      # Module
      m_name = name[0]
      # Controller
      c_name = name[1]

      puts "**************** #{m_name}: #{c_name}"

      write_permission(m_name, c_name, 'access', 'Gerenciar')

      methods(controller).each do |method|
        if method =~ /^([A-Za-z\d*]+)+([\w]*)+([A-Za-z\d*]+)$/ #add_user, add_user_info, Add_user, add_User
          name, cancan_action = eval_cancan_action(method)
          write_permission(m_name, c_name, cancan_action, name)
        end
      end
    end
  end
end

def controllers
  removed = [ApplicationController, Devise::RegistrationsController, CrudController,
             HomeController]

  arr = []

  # Load all controllers
  controllers = Dir.new("#{Rails.root}/app/controllers").entries

  controllers.each do |entry|
    if entry =~ /_controller/
      #check if the controller is valid
      arr << entry.camelize.gsub('.rb', '').constantize
    elsif entry =~ /^[a-z]*$/ #namescoped controllers
      Dir.new("#{Rails.root}/app/controllers/#{entry}").entries.each do |x|
        if x =~ /_controller/
          arr << "#{entry.titleize}::#{x.camelize.gsub('.rb', '')}".constantize
        end
      end
    end
  end

  arr - removed
end

def methods(controller)
  removed = %w(modal show new edit)
  controller.action_methods - ApplicationController.action_methods - removed
end

def controller_name(controller)
  underscore(controller.to_s.gsub('Controller', '')).split('/')
end

#this method returns the cancan action for the action passed.
def eval_cancan_action(action)
  case action.to_s
    when "index"
      cancan_action = "read"
    else
      cancan_action = action.to_s
  end

  name = I18n.t("actions.#{action}")
  return name, cancan_action
end

#check if the permission is present else add a new one.
def write_permission(modulus, controller, cancan_action, name)
  permission = Permit.where(controller: controller, action: cancan_action).first

  unless permission
    permission = Permit.new
    permission.name = name
    permission.modulus = modulus
    permission.controller = controller
    permission.action = cancan_action
    permission.save!
  end
end

def underscore(string)
  string.gsub(/::/, '/').
    gsub(/([A-Z]+)([A-Z][a-z])/, '\1_\2').
    gsub(/([a-z\d])([A-Z])/, '\1_\2').
    tr("-", "_").
    downcase
end
