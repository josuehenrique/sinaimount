class TreeTreatment
  def self.create_structure(*attr)
    new(*attr).create_structure
  end

  def initialize(objects)
    @objects = objects
    @data = {}
    @pluralized = objects.first.class.model_name.plural unless objects.blank?
  end

  def create_structure
    @objects.roots.each do |root|
      crete_root root

      root.self_and_descendants.each do |object|
        next unless object.parent_id

        create_child object
      end
    end

    @data
  end

  protected

  def crete_root(root)
    add_element(@data, root)
    child_structure(@data[root.id])
  end

  def create_child(object)
    # Find the related node at tree
    obj = @data.deep_find(object.parent_id)

    child_structure(obj)
    add_element(obj['additionalParameters']['children'], object)
  end

  # This structure allow the element to receive children
  def child_structure(recipient)
    recipient['additionalParameters'] ||= {'children' => {}} unless recipient.has_key?('additionalParameters')
  end

  def add_element(recipient, object)
    link = "#{object.to_s}
           <div class='pull-right action-buttons'>
            <a href='#{@pluralized}/#{object.id}/edit' class='blue' title='Editar'><i class='icon-pencil bigger-130'></i></a>
            <a href='#{@pluralized}/#{object.id}' data-confirm='Você tem certeza?' data-method='delete' class='red' title='Deletar'><i class='icon-trash bigger-130'></i></a>
           </div>"
    recipient[object.id] = {name: link, type: type(object)}
  end

  def type(object)
    object.leaf? ? 'item' : 'folder'
  end
end
