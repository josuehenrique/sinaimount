module ActiveRecord
  module SearchAttributes
    extend ActiveSupport::Concern

    included do
      class_attribute :_search_attributes
    end

    module ClassMethods
      def attr_search(*attributes)
        self._search_attributes = Set.new(attributes.map { |a| a.to_s }) + (self._search_attributes || [])
      end

      def search_attributes
        self._search_attributes
      end
    end
  end
end
