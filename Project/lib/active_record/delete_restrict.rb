module ActiveRecord
  module DeleteRestrict
    def destroy
      super
    rescue ActiveRecord::DeleteRestrictionError
      errors.add(:base, :cant_be_destroyed)
      false
    end
  end
end
