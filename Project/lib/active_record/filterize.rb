module ActiveRecord
  module Filterize
    extend ActiveSupport::Concern

    module ClassMethods
      def filterize(param=nil)
        scope :active_filter, lambda{|active=true| where(active: active)}
      end
    end
  end
end
