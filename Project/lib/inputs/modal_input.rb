module Inputs
  class ModalInput < SimpleForm::Inputs::Base
    def input
      addon_search {modal_field + hidden_field + modal_link}
    end

    protected

    def input_id
      sanitized_object_name + '_' + model_name + '_id'
    end

    def modal_link
      "<a id='#{input_id}_link' data-remote=true data-toggle='modal' href='#{modal_url}?input_id=#{input_id}' data-target='modal-window'></a>".html_safe
    end

    def modal_field
      @builder.text_field(label_target, input_html_options)
    end

    def hidden_field
      @builder.hidden_field(hidden_field_name) if hidden_field_name
    end

    def label_target
      reflection ? reflection.name : attribute_name
    end

    def input_html_classes
      super.unshift(:string)
    end

    def input_html_options
      super.tap do |options|
        association = @builder.object.class.
        reflect_on_all_associations(:belongs_to).select {
          |a| a.foreign_key.to_sym == attribute_name.to_sym
        }.last

        options[:value]                 ||= association ? @builder.object.send(association.name) : ''
        options['data-modal-url']       ||= modal_url
        options['data-hidden-field-id'] ||= hidden_field_id if hidden_field_id
        options['readonly']             ||= true
        options['placeholder']          ||= 'Clique para pesquisar...'
      end
    end

    def modal_url
      options[:modal_url] || find_modal_url
    end

    def find_modal_url
      route = "modal_#{model_name.pluralize}_path"

      unless template.respond_to?(route)
        raise "Missing route for #{model_name.gsub(/_/, " ")} modal (#{route})"
      end

      template.send(route)
    end

    def model_name
      name = reflection ? reflection.klass.model_name : real_attribute(attribute_name)
      name.to_s.underscore
    end

    def real_attribute(attribute)
      if attribute.to_s.end_with?('_id')
        attribute.to_s.split('_id').first
      else
        attribute
      end
    end

    def hidden_field_name
      options.fetch(:hidden_field, attribute_name)
    end

    def hidden_field_id
      [sanitized_object_name, index, hidden_field_name].compact.join('_') if hidden_field_name
    end

    def index
      @builder.options[:index]
    end

    def sanitized_object_name
      object_name.gsub(/\]\[|[^-a-zA-Z0-9:.]/, "_").sub(/_$/, "")
    end

    private

    def addon_search
      addon("<i class='icon-search'></i>") {yield}
    end

    def addon(icon)
      "<div class='input-prepend'><span class='add-on modal_input_icon_search'>" + icon + "</i></span>" + yield + "</div>"
    end
  end
end
