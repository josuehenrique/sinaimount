class UnitRelations
  attr_accessor :business_unit, :selected, :relation, :repository

  def self.update(*attr)
    new(*attr).update
  end

  def initialize(business_unit_id, selected, relation, repository = Relacoesun, un_repository = Un)
    self.business_unit = un_repository.find(business_unit_id)
    self.selected = selected.blank? ? [] : relation.find(selected)
    self.relation = relation
    self.repository = repository
  end

  def update
    missing.each do |rel|
      repository.create(un: business_unit, relacionado: rel)
    end

    links_to_delete.each do |rel|
      rel.destroy
    end
  end

  protected

  def current
    business_unit.send(relation_method)
  end

  def available
    relation.ordered
  end

  def missing
    selected - current
  end

  def to_remove
    available - selected
  end

  def links_to_delete
    repository.where(relacionado_type: relation.to_s, relacionado_id: to_remove.collect(&:id))
  end

  def relation_method
    relation.model_name.to_s.downcase.pluralize
  end
end
