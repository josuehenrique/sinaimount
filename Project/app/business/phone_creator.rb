# Verifica se o objeto já possui os telefones, caso não tenha, salva
class PhoneCreator
  def self.do(*attr)
    new(*attr).do
  end

  # @param [Class] source Objeto principal que está sendo criado
  # @param [Class] object Registro ao qual o telefone será vinculado
  # @param [Array] phones [{number: '6291658544', classification: 'landline'}]
  def initialize(source, object, phones)
    @source = source
    @object = object
    @phones = phones
  end

  def do
    phones.each do |attr|
      classification = attr[:classification]
      number = attr[:number]

      if new_number?(object, number)
        phone = object.phones.new(number: number, classification: classification)
        unless phone.save
          source.errors.add(:base, phone.errors.full_messages.to_sentence)
          return false
        end
      end
    end

    true
  end

  private

  def new_number?(object, number)
    object.phones.by_number(number).blank?
  end

  attr_reader :object, :phones, :source
end
