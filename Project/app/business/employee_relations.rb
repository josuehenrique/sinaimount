class EmployeeRelations
  attr_accessor :employee, :selected, :relation, :repository

  def self.update(*attr)
    new(*attr).update
  end

  def self.grant_single_permission(*attr)
    new(*attr).grant_single_permission
  end

  def initialize(employee_id, selected, relation, repository = Relacoesfuncionario, employee_repository = Funcionario)
    self.employee = employee_repository.find(employee_id)
    self.selected = selected.blank? ? [] : relation.find(selected)
    self.relation = relation
    self.repository = repository
  end

  def update
    missing.each do |rel|
      grant_permission(rel)
    end

    links_to_delete.each do |rel|
      rel.destroy
    end
  end

  def grant_single_permission
    grant_permission(selected)
  end

  protected

  def grant_permission(rel)
    repository.create(funcionario: employee, relacionado: rel)
  end

  def current
    employee.send(relation_method)
  end

  def available
    relation.ordered
  end

  def missing
    selected - current
  end

  def to_remove
    available - selected
  end

  def links_to_delete
    repository.where(relacionado_type: relation.to_s, relacionado_id: to_remove.collect(&:id))
  end

  def relation_method
    relation.model_name.to_s.downcase.pluralize
  end
end
