class GenerateDefaultConfiguration
  def self.do!
    new.do!
  end

  def do!
    token
  end

  protected

  def token
    if not_created?(:token)
      config = create_config('Chave (Token)', 'Chave utilizada na criptografia de senhas', ConfigurationType::TOKEN,
                             Modulus::ENTERPRISE)
      create_value(config, '4das44!@*(*!)#dASd*638**91%@#', ConfigurationValueType::STRING)
    end
  end

  private

  def not_created?(config)
    Configuracao.send(config) ? false : true
  end

  def create_config(name, description, ident, modulus)
    Configuracao.create!(nome: name, descricao: description, modulus: modulus) do |config|
      config.ident = ident
    end
  end

  def create_value(config, valor, tipo, titulo = nil)
    config.configuracao_valores.build(valor: valor) do |v|
      v.tipo = tipo
      v.titulo = titulo
    end
    config.save!
  end
end
