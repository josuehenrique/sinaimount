class Zipcode
  def self.request_address(*attr)
    new(*attr).request_address
  end

  def initialize(zipcode, options = {})
    @zipcode = zipcode
    @city_repository = options.delete(:city_repository) { City }
    @state_repository = options.delete(:staet_repository) { State }
    @address_searcher = options.delete(:address_searcher) { BuscaEndereco }
  end

  def request_address
    address = {}

    address_search = address_searcher.cep(zipcode)

    address[:address] = "#{address_search[:tipo_logradouro]} #{address_search[:logradouro]}".upcase

    address[:neighborhood] = address_search[:bairro].upcase

    state = search_state address_search[:uf]
    address[:state_id] = state.id if state

    city = search_city address_search[:cidade]
    address[:city_id] = city.id if city

    address
  rescue
    {}
  end

  private

  attr_reader :zipcode, :city_repository, :state_repository, :address_searcher

  def search_city(name)
    city_repository.find_by(name: name)
  end

  def search_state(acronym)
    state_repository.find_by(acronym: acronym)
  end
end
