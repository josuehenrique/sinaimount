module Remote
  class Alias
    def self.create(*attr)
      new(*attr).create
    end

    def self.destroy(*attr)
      new(*attr).destroy
    end

    def initialize(source, destiny, settings = Settings, dominio_repository = Dominio, servidor_repository = Servidor)
      raise I18n.t('messages.enviroment_configured_to_not_use_remote_connection') if !settings.remote_connection

      @source = source
      @destiny = destiny
      @dominio = dominio_repository.find_by_nome(source.split('@').last)

      @server = servidor_repository.email(@dominio.id)
    end

    def create
      ActiveRecord::Base.transaction do
        connect_email_alias
        account_alias = remote_alias_repository.new
        account_alias.origem = @source
        account_alias.destino = @destiny
        account_alias.dominio = @dominio.nome
        account_alias.save!
      end
    end

    def destroy
      ActiveRecord::Base.transaction do
        connect_email_alias
        account_alias = remote_alias_repository.where(origem: @source, destino: @destiny)
        account_alias.each { |a| a.destroy }
      end
    end

    protected

    def remote_alias_repository(repository = RemoteAlias)
      repository
    end

    def connect_email_alias
      ConnectServer.email(:alias, @server)
    end
  end
end
