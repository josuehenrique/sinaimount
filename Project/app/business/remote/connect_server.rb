module Remote
  class ConnectServer
    def self.email(*attr)
      new(*attr).email
    end

    def self.radius(*attr)
      new(*attr).radius
    end

    def initialize(table, server)
      @server = server
      @table = table
    end

    def email
      case @table
        when :alias then object = remote_alias_repository
        when :email_account then object = remote_email_account_repository
        else
          raise 'Invalid remote service!'
      end

      connect_to(object, 'email')

      object
    end

    def radius(db = 'radius')
      case @table
        when :group_check then object = remote_group_check_repository
        when :group_reply then object = remote_group_reply_repository
        else
          raise 'Invalid remote service!'
      end

      connect_to(object, db)

      object
    end

    protected

    def connect_to(object, db)
      object.establish_connection({adapter: 'mysql2', host: @server.ip,
                                   database: db, username: @server.usuario,
                                   password: @server.password})
    end

    def remote_alias_repository(repository = RemoteAlias)
      repository
    end

    def remote_email_account_repository(repository = RemoteEmailAccount)
      repository
    end

    def remote_group_check_repository(repository = RemoteGroupCheck)
      repository
    end

    def remote_group_reply_repository(repository = RemoteGroupReply)
      repository
    end
  end
end
