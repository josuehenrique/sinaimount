module Remote
  class Email
    def self.create(*attr)
      new(*attr).create
    end

    def self.update(*attr)
      new(*attr).update
    end

    def self.destroy(*attr)
      new(*attr).destroy
    end

    def initialize(email, settings = Settings, server_repository = Servidor)
      raise I18n.t('messages.enviroment_configured_to_not_use_remote_connection') if !settings.remote_connection

      @email = email
      @dominio = @email.dominio
      @server = server_repository.email(@dominio.id)
    end

    def create
      ActiveRecord::Base.transaction do
        create_remote_email_account
        remote_alias_class.create(@email.full, @email.full)
        script_create

        @email.encrypt_password
      end
    end

    def update
      ActiveRecord::Base.transaction do
        connect_email_account
        account = find_remote_account
        account.senha = encrypt_password(@email.senha)
        account.quota = @email.quota
        account.save!
      end
    end

    def change_situation(active)
      ActiveRecord::Base.transaction do
        connect_email_account
        account = find_remote_account
        account.ativo = active
        account.save!
      end
    end

    def destroy
      ActiveRecord::Base.transaction do
        connect_email_account
        connect_email_alias

        account = find_remote_account

        script_destroy

        if account
          account.destroy
          account.aliases.each {|f| f.destroy}
        end
      end
    end

    protected

    def encrypt_password(password)
      %x{echo #{password} |openssl passwd -1 -stdin}.gsub("\n","")
    end

    def connect_email_account
      ConnectServer.email(:email_account, @server)
    end

    def connect_email_alias
      ConnectServer.email(:alias, @server)
    end

    def find_remote_account
      remote_email_account_repository.find_by_usuario(@email.full)
    end

    def create_remote_email_account
      connect_email_account
      account = remote_email_account_repository.new
      account.usuario = @email.full
      account.senha = encrypt_password(@email.senha)
      account.dominio = @dominio.nome
      account.home = "/email/" + @dominio.nome + "/" + @email.usuario + "/"
      account.maildir = "Maildir"
      account.quota = @email.quota
      account.ativo = 1
      account.uid = configuration_repository.email_uid
      account.gid = configuration_repository.email_gid
      account.save!
    end

    def script_create
      ssh_class.start(@server.ip, @server.usuario, password: @server.password) do |ssh|
        ssh.exec!("sudo /home/SinaiMount/./criar_conta_email.sh #{@email.usuario} #{@dominio.nome}")
      end
    end

    def script_destroy
      ssh_class.start(@server.ip, @server.usuario, password: @server.password) do |ssh|
        ssh.exec!("sudo /home/SinaiMount/./deletar_conta_email.sh #{@email.usuario} #{@dominio.nome}")
      end
    end

    private

    def configuration_repository(repository = Configuracao)
      repository
    end

    def remote_email_account_repository(repository = RemoteEmailAccount)
      repository
    end

    def remote_alias_class(klass = Remote::Alias)
      klass
    end

    def ssh_class(klass = Net::SSH)
      klass
    end
  end
end
