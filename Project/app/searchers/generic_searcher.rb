class GenericSearcher
  def self.search(*attributes)
    new(*attributes).search
  end

  def initialize(klass, params, collection)
    @klass = klass

    @attr = params[:search][:attr]
    @keyword = params[:search][:keyword]
    @page = params[:page]
    @active = params[:active].blank?

    @collection = collection
  end

  def search
    if is_foreign_key?
      scope = @collection.includes(fk_relation).where("#{fk_relation_table}.id = #{@keyword}" )
    elsif @attr == 'id'
      scope = @collection.where("#{@attr} = '#{@keyword}'")
    else
      scope = @collection.where("#{@attr} LIKE ?", '%' + @keyword + '%')
    end

    scope = scope.active_filter(@active) if @klass.respond_to?(:active_filter)

    scope.ordered.paginate(per_page: 10, page: @page)
  end

  protected

  def is_foreign_key?
    @attr.end_with?('_id')
  end

  def fk_relation
    @attr.gsub('_id', '')
  end

  def fk_relation_table
    @klass.reflections[fk_relation.to_sym].class_name.underscore.downcase.pluralize
  end
end
