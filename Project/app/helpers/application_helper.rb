module ApplicationHelper
  def simple_form_for(object, *args, &block)
    options = args.extract_options!
    options[:builder] ||= Base::FormBuilder

    super(object, *(args << options), &block)
  end

  def simple_form_css_class(record, options)
    [super, dom_class(record)]
  end

  def submenu(controller, modulu = nil)
    @name = I18n.t("controllers.#{controller}")

    @path = modulu ? "#{modulu}_#{controller}_path" : "#{controller}_path"

    render 'layouts/submenu', {controller: controller}
  end

  def area_field(value)
    "#{number_with_precision(value)} m2"
  end

  def smart_report_path
    url_for controller: controller_name, action: :show, id: 'report', only_path: true
  end

  def controller_asset?
    Rails.application.assets.find_asset controller_name
  end

  # Apply I18n::Alchemy on a collection of active record objects.
  #
  # Usage:
  #
  #   localized(products) do |product|
  #     product.price #=> "1,50"
  #   end
  #
  #   products = localized(products)
  #   products.first.price #=> "1,50"
  def localized(collection_object)
    if block_given?
      collection_object.each { |object| yield(object.localized) }
    else
      collection_object.map { |object| object.localized }
    end
  end

  def real_attribute(attribute)
    if attribute.to_s.end_with?('_id')
      attribute.to_s.split('_id').first
    else
      attribute
    end
  end

  def localized_attribute(klass, attribute_name)
    klass.human_attribute_name real_attribute(attribute_name)
  end

  def options_for_input(object_setting)
    options = {}
    if object_setting
      options.merge!(label: object_setting.name)
      options.merge!(collection: object_setting.options.map(&:name)) if object_setting.collection?
      options.merge!(as: :boolean) if object_setting.boolean?
      options.merge!(as: :date) if object_setting.date?
      options.merge!(as: :datetime) if object_setting.datetime?
      options[:input_html] ||= {}
      options[:input_html]['data-attribute-name'] = object_setting.name.parameterize.to_s
      if object_setting.respond_to?(:dependency) && object_setting.dependency.present?
        options[:input_html]['data-dependency'] = object_setting.dependency.name.parameterize.to_s
        options[:input_html][:disabled] = :disabled
      end
    end
    options
  end

  def find_input_for(value, setting)
    Base::FindInput.new(value, setting).find
  end

  def value_by_field_type(value)
    klass = value.class
    if klass == TrueClass
      "<i class='icon-ok-sign green-icons'></i>".html_safe
    elsif klass == FalseClass
      "<i class='icon-remove-sign red-icons'></i>".html_safe
    else
      value.nil? ? '-' : value
    end
  end

  def mustache(name, &block)
    content_tag(:script, id: name, type: 'text/x-mustache') do
      content = capture(&block)
      content = content.gsub("</script>", "<\\/script>")
      content.html_safe
    end
  end

  def menu_id(key)
    path = url_for(only_path: true).split('/')

    return if path.size < 3

    case key
      when :sub then
        submenu = path.delete_at(2)

        if %w(relacoesfuncionarios).include? submenu
          submenu = 'funcionarios'
        else
          %w(Funcionario Un Setor Cargo Cliente).each do |item|
            submenu = item.downcase.pluralize if params.values.include?(item)
          end
          submenu = 'estados' if %w(cidades).include? submenu
          submenu = 'uns' if %w(relacoesuns tipooses_uns).include? submenu
          submenu = 'servidores' if %w(servidoresservicosdominios).include? submenu
        end

        'sub_' + submenu
      when :menu then
        menu = path.delete_at(1)
        menu = 'enterprise' if (params.values & %w(Funcionario Un Setor)).size > 0
        menu = 'crm' if (params.values & %w(Cliente)).size > 0

        'menu_' + menu
    end
  end
end
