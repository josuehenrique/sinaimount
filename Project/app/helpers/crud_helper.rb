module CrudHelper
  def model_file
    resource_class.model_name.downcase
  end

  def plural(klass = resource_class)
    klass.model_name.human(count: 'many')
  end

  def singular(klass = resource_class)
    klass.model_name.human
  end

  def paginate(params)
    will_paginate collection(params) if collection(params).respond_to?(:total_pages)
  end

  def remote_paginate(params)
    will_paginate collection(params), renderer: RemotePaginator::LinkRenderer if collection(params).respond_to?(:total_pages)
  end

  # Get modal attributes and intersect with +params[:attributes]+ if exists
  def attributes
    attributes = resource_class.modal_attributes
    attributes &= params[:attributes].split(',') if params[:attributes]

    associations = resource_class.reflect_on_all_associations
    attributes.delete("ativo")
    attributes.map do |attribute|
      next if attribute.blank?
      association = associations.detect { |association| association.foreign_key == attribute }

      # If attribute is an association and is not polymorphic, use the association
      # name instead of the foreign key.
      if association
        association.name unless association.options.include? :polymorphic
      else
        attribute
      end
    end.compact
  end

  def show_attribute(attribute, object = resource, value = nil)
    show = "<b>#{localized_attribute(object.class, attribute)}</b>: "
    show += value || value_by_field_type(formatted_attribute(object, attribute)).to_s
    show += "<br>"

    show.html_safe
  end

  def formatted_attribute(resource, attribute)
    attribute = real_attribute(attribute)
    value = resource.send(attribute)

    case value.class.to_s
      when 'String' then
        case attribute.to_s
          when 'cep' then Mask.cep(value)
          when 'cpf' then Mask.cpf(value)
          when 'cnpj' then Mask.cnpj(value)
          when 'telefone1', 'telefone2', 'telefone3' then Mask.tel(value)
          else value
        end
      when 'ActiveSupport::TimeWithZone' then Mask.date_time(value)
      when 'BigDecimal' then Mask.real(value)
      when 'Date' then Mask.date(value)
      when 'FalseClass' then "<i class='icon-remove-sign red'></i>".html_safe
      when 'TrueClass' then "<i class='icon-ok-sign green'></i>".html_safe
      else value
    end
  end

  def exist_route?(route, environment = {})
    path = Rails.application.routes.recognize_path(route, environment)
    path[:action] != 'render_error'
  end

  def create?
    begin
      can?(:create, controller_name) && exist_route?(new_resource_path)
    rescue NoMethodError
      false
    end
  end

  def create_link
    link_to raw("<i class='icon-plus'></i> "+ t('buttons.new', resource: singular, cascade: true)), new_resource_path, class: ' btn btn-large btn-primary'
  end

  def show_link(object)
    if can? :read, object
      link_to raw("<i class='icon-eye-open bigger-130 blue'></i>"), resource_path(object), 'data-placement' => :top, 'data-rel' => :tooltip,
              class: 'blue', title: (I18n.t 'actions.show')
    end
  end

  def edit_link(object)
    if can? :update, object
      link_to raw("<i class='icon-cog bigger-130 grey'></i>"), edit_resource_path(object), 'data-placement' => :top, 'data-rel' => :tooltip,
              class: 'green', title: (I18n.t 'actions.edit')
    end
  end

  def destroy_link(object)
    if (can? :manage, object) && exist_route?(resource_path(object), method: :delete)
     link_to raw("<i class='icon-trash bigger-130'></i>"), resource_path(object), method: :delete,
             'data-placement' => :top, 'data-rel' => :tooltip, class: 'red tooltip-error',
             title: (I18n.t 'actions.delete'), data: { confirm: (I18n.t '.are_you_sure') }
    end
  end

  def situation_change_link(object)
    return destroy_link(object) unless object.respond_to?(:ativo) || object.respond_to?(:active)

    ativo = object.ativo if object.respond_to?(:ativo)
    ativo = object.active if object.respond_to?(:active)

    if ativo
      if can? :inactivate, object
        link_to raw("<i class='icon-minus bigger-130'></i>"), inactivate_resource_path(object),
                'data-placement' => :top, 'data-rel' => :tooltip,
                class: 'red', title: (I18n.t 'actions.inactivate'), data: {confirm: (I18n.t '.are_you_sure')}
      end
    else
      if can? :activate, object
        link_to raw("<i class='icon-ok bigger-130 green'></i>"), activate_resource_path(object),
                'data-placement' => :top, 'data-rel' => :tooltip,
                class: 'orange', title: (I18n.t 'actions.activate'), data: {confirm: (I18n.t '.are_you_sure')}
      end
    end
  end

  def activate_resource_path(object)
    edit_resource_path(object).gsub('edit', 'activate')
  end

  def inactivate_resource_path(object)
    edit_resource_path(object).gsub('edit', 'inactivate')
  end

  def info_box(title, span = 'span12')
    content_tag :div, class: 'row-fluid' do
       (content_tag :div, class: "block #{span}" do
         header = content_tag :p, class: 'block-heading' do
          title
        end

        body = (content_tag :div, class: 'block-body form-horizontal  wizard' do
          yield
        end)
         header + body
       end)
    end
  end

  def pill
    content = (content_tag :div, class: 'col-sm-12' do
      content_tag :ul, class: 'nav nav-pills pill-top' do
        yield
      end
    end)

    content + '<br><br><br>'.html_safe
  end

  def list_table
    content_tag :div, class: 'col-xs-12' do
      content_tag :div, class: 'table-responsive' do
        content_tag :div, class: 'dataTables_wrapper' do
          content_tag :div, class: 'row' do
            yield
          end
        end
      end
    end
  end

  def selectable_boxes
    content_tag :div, class: 'col-xs-6' do
      content_tag :div, class: 'pre-scrollable' do
        content_tag :table, class: 'table table-striped table-bordered table-hover' do
          yield
        end
      end
    end
  end

  def search_params(params)
    params_for_search = params.dup
    [:action, :controller, :search, :utf8].each do |param|
      params_for_search.delete param
    end
    params_for_search
  end

  def icons
    content_tag :div, class: 'visible-md visible-lg hidden-sm hidden-xs action-buttons' do
      yield
    end
  end

  def icon_1
    "width=10px"
  end

  def icon_2
    "width=65px"
  end

  def icon_3
    "width=90px"
  end

  def icon_4
    "width=110px"
  end

  #spans

  def span_date
    'width: 6% !important'
  end

  def span_5
    'width: 5% !important'
  end

  def span_10
    'width: 10% !important'
  end

  def span_15
    'width: 15% !important'
  end

  def span_20
    'width: 20% !important'
  end

  def span_25
    'width: 25% !important'
  end

  def span_30
    'width: 30% !important'
  end

  def span_35
    'width: 35% !important'
  end

  def span_40
    'width: 40% !important'
  end

  def span_45
    'width: 45% !important'
  end

  def span_50
    'width: 50% !important'
  end

  def span_55
    'width: 55% !important'
  end

  def span_60
    'width: 60% !important'
  end
end
