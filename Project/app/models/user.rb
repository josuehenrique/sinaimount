class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  devise :database_authenticatable, :registerable, :timeoutable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessible :login, :email, :password, :password_confirmation,
                  :funcionario_id, :roles_attributes

  attr_protected :administrator

  attr_list :login, :email, :funcionario_id

  attr_search :login, :email

  belongs_to :funcionario
  has_many :roles, as: :related, dependent: :destroy

  delegate :uns, :filas, :contabancarias, :caixinhas, :estoques,
           to: :funcionario, allow_nil: true

  accepts_nested_attributes_for :roles, allow_destroy: true

  validates :login, :email, presence: true
  validates :password, presence: true, on: :create

  orderize :id

  def to_s
    login
  end

  def build_role(attributes)
    roles.build(attributes)
  end

  def delete_role(role)
    roles.delete(role)
  end
end
