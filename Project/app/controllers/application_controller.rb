class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_filter :authenticate_user!

  unless Rails.application.config.consider_all_requests_local
    rescue_from Exception, with: lambda { |exception| render_error 500, exception }
  end

  rescue_from CanCan::Unauthorized, with: lambda { render_error 401 }
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  def current_employee
    current_user.funcionario
  end

  def module_name
    url_for(only_path: true).split('/')[1]
  end

  def render_error(status = 404, exception = nil)
    if request.xhr? && status == 401
      render :js => "alert('Acesso negado!')"
    else
      respond_to do |format|
        format.html { render file: "#{Rails.root}/public/#{status}", status: status }
        format.all { render nothing: true, status: status }
      end
    end
  end

  def cities_of_state
    unless params[:state_id].blank?
      @cities = City.by_state(params[:state_id]).map { |c| [c.name, c.id] }.insert(0, ['Selecione', ''])
      render partial: 'shared/cities_of_state'
    end
  end

  def show_address
    @values = Zipcode.request_address(params[:zipcode])
    @cities = City.by_state(@values[:state_id]).map { |c| [c.name, c.id] }.insert(0, ['Selecione', ''])
    render partial: 'crud/show_address'
  end

  protected

  def open_modal(view = params[:action])
    render partial: 'crud/open_modal', locals: {view: view}
  end

  def errors(object)
    list = object.errors.full_messages
    return nil if list.empty?
    list.join('<br>')
  end

  def authorize_resource!
    authorize! action_name, controller_name
  end

  def after_sign_out_path_for(resource)
    new_user_session_path
  end

  def record_not_found
    redirect_to(collection_path, alert: (t 'messages.record_not_found'))
  end
end
