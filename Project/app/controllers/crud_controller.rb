class CrudController < ApplicationController
  before_filter :authorize_resource!

  inherit_resources

  respond_to :js, :json

  has_scope :page, default: 1, only: [:index, :modal], unless: :disable_pagination?
  has_scope :per_page, default: 10, only: [:index, :modal], unless: :disable_pagination?

  def index(options={}, &block)
    params[:page] = 'all' unless params[:select_id].blank?
    params[:search] ||= {}
    build_resource
  end

  def create
    create! { collection_path }
  end

  def update
    update! { resource_path }
  end

  def destroy
    destroy! do |success, failure|
      failure.html do
        flash.keep
        redirect_to collection_path
      end
    end
  end

  def activate
    activate!
  end

  def inactivate
    inactivate!
  end

  def modal
    build_resource
    render partial: 'open_modal'
  end

  protected

  def inactivate!(redirect_params = {}, &block)
    if resource.respond_to?(:ativo)
      resource.ativo = false
    else
      resource.active = false
    end

    if resource.save
      block.call if block_given?
      redirect_to collection_path(redirect_params), notice: t('messages.inactivated')
    else
      redirect_to collection_path(redirect_params), alert: t('messages.cant_be_inactivated')
    end
  end

  def activate!(redirect_params = {}, &block)
    if resource.respond_to?(:ativo)
      resource.ativo = true
    else
      resource.active = true
    end

    if resource.save
      block.call if block_given?
      redirect_to collection_path(redirect_params), notice: t('messages.activated')
    else
      redirect_to collection_path(redirect_params), alert: t('messages.cant_be_activated')
    end
  end

  def smart_resource_path
    path = nil
    if respond_to? :show
      path = resource_path rescue nil
    end
    path ||= smart_collection_path
  end

  helper_method :smart_resource_path

  def smart_collection_path
    path = nil
    if respond_to? :index
      path ||= collection_path rescue nil
    end
    if respond_to? :parent
      path ||= parent_path rescue nil
    end
    path ||= root_path rescue nil
  end

  helper_method :smart_collection_path

  # Build resource using I18n::Alchemy
  def build_resource
    get_resource_ivar || set_resource_ivar(effectively_build_resource)
  end

  # Effectively build resource using I18n::Alchemy
  def effectively_build_resource
    end_of_association_chain.send(method_for_build).tap do |object|
      object.localized.assign_attributes(*resource_params)
    end
  end

  # Update resource using I18n::Alchemy
  def update_resource(object, attributes)
    object.localized.update_attributes(*attributes)
  end

  # Get collection using ordered scope
  def collection(params)
    if params[:search].blank? || params[:search][:keyword].blank?
      get_collection_ivar || set_collection_ivar(collection_records)
    else
      searcher
    end
  end

  def collection_records
    resource_class.respond_to?(:active_filter) ? end_of_association_chain.ordered.active_filter(params[:active] || true) : end_of_association_chain.ordered
  end

  def searcher
    GenericSearcher.search(resource.class, params, end_of_association_chain)
  end

  def disable_pagination?
    params[:page] == 'all'
  end
end
