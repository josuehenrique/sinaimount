class HomeController < ApplicationController
  skip_before_filter :authenticate_user!, only: [:boleto_caixa]

  def index
  end

  def boleto_caixa
    headers['Content-Type']='application/pdf'

    boletos = []

    params[:boletos].values.each do |dados_boleto|
      dados_boleto['data_documento'] = dados_boleto['data_documento'].to_date

      boletos << Brcobranca::Boleto::Caixa.new(dados_boleto)
    end

    send_data Brcobranca::Boleto::Base.lote(boletos), filename: "boleto.pdf"
  end

  protected

  def boleto_dados
    {cedente: "Kivanio Barbosa",
     documento_cedente: "12345678912",
     sacado: "Claudio Pozzebom",
     sacado_documento: "12345678900",
     valor: "135.00",
     agencia: "4042",
     conta_corrente: "61900",
     convenio: "1238798",
     numero_documento: "7777700168",
     dias_vencimento: "5",
     data_documento: "2008-02-01",
     instrucao1: "Pagável na rede bancária até a data de vencimento.",
     instrucao2: "Juros de mora de 2.0% mensal(R$ 0,09 ao dia)",
     instrucao3: "DESCONTO DE R$ 29,50 APÓS 05/11/2006 ATÉ 15/11/2006",
     instrucao4: "NÃO RECEBER APÓS 15/11/2006",
     instrucao5: "Após vencimento pagável somente nas agências do Banco do Brasil",
     instrucao6: "ACRESCER R$ 4,00 REFERENTE AO BOLETO BANCÁRIO",
     sacado_endereco: "Av. Rubéns de Mendonça, 157 - 78008-000 - Cuiabá/MT"}
  end
end
