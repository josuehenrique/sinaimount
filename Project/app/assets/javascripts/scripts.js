jQuery.ajaxSetup({
  'beforeSend': function(xhr) { xhr.setRequestHeader("Accept", "text/javascript") }
});

function checkboxes_all(sender, targets){
  $(sender).click(function() {
    if(this.checked) {
      $(targets).each(function() {
        this.checked = true;
      });
    }
    else {
      $(targets).each(function() {
        this.checked = false;
      });
    }
  });
}

//custom checkbox and radio button
$(document).ready(function(){
  $('input').iCheck({
    checkboxClass: 'icheckbox_flat-red',
    radioClass: 'iradio_flat-red'
  });
});

$(document).ready(function () {
  // Link to new object
  $.each($('select'), function( index, value ) {
    var el = $('#'+value.id),
      link = el.attr('link_to_new'),
      location = el.parent();

    if (link) {
      location.append("" +
        "<a href="+link+" data-remote='true' data-target='modal-window' data-toggle='modal'>" +
        "<i class='icon-expand-alt bigger-200'></i></a>");
    }
  });
});

// Functions TO NOT USE on TEST environment
  $('.date-picker').datepicker({
    format: 'dd/mm/yyyy',
    language: 'pt-BR',
    todayHighlight: true
  });

//Icons for fields
$( "<span class='add'><i class='icon-calendar'></i></span>" ).insertBefore(".date-picker");
$( "<span class='add'><i class='icon-phone'></i></span>" ).insertBefore(".mask-phone");
$( "<span class='add'><i class='icon-user'></i></span>" ).insertBefore(".field-user");
$( "<span class='add'><i class='icon-envelope'></i></span>" ).insertBefore(".mask-zipcode");
$( "<span class='add'><i class='icon-map-marker'></i></span>" ).insertBefore(".field-coordinate");

// Remove mask on submit
$('form').submit(function () {
  $('.mask-zipcode').mask("99999999");
  $('.mask-cnpj').mask("99999999999999");
  $('.mask-cpf').mask("99999999999");

  unmask_phone($('.mask-phone'));

  $('input.decimal').each(function( index, value ) {
    ($('#' + value.id)).unpriceFormat();
  });
});

//Add mask
function addMasks() {
  $('.mask-zipcode').mask("99.999-999");
  $('.mask-cnpj').mask("99.999.999/9999-99");
  $('.mask-cpf').mask("999.999.999-99");
  $('.date-picker').mask("99/99/9999");
  $('.mask-acronym').mask("aa");

  mask_phone($('.mask-phone'));
}

addMasks();
