require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(:default, Rails.env)

module SinaiMount
  class Application < Rails::Application
    config.to_prepare { Devise::SessionsController.layout 'login' }

    # Custom directories with classes and modules you want to be autoloadable.
    config.autoload_paths += %W(
      #{config.root}/lib
      #{config.root}/app/business
      #{config.root}/app/searchers
    )

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Brasilia'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    config.i18n.load_path += Dir["#{config.root}/config/locales/**/*.yml"]
    config.i18n.default_locale = 'pt-BR'

    I18n.enforce_available_locales = true

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password]

    # Enable escaping HTML in JSON.
    config.active_support.escape_html_entities_in_json = true

    # Enforce whitelist mode for mass assignment.
    # This will create an empty whitelist of attributes available for mass-assignment for all models
    # in your app. As such, your models will need to explicitly whitelist or blacklist accessible
    # parameters by using an attr_accessible or attr_protected declaration.
    #config.active_record.whitelist_attributes = false

    # Include helpers from current controller only
    config.action_controller.include_all_helpers = false

    config.active_record.disable_implicit_join_references = false

    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '1.0'

    config.assets.enabled = true

    config.assets.precompile += ['base/jquery-1.9.1.js']

    css_files = []
    Dir.glob("#{config.root}/app/assets/stylesheets/*.{css, erb}").each { |f|
      css_files << File.basename(f)
    }

    js_files = []
    Dir.glob("#{config.root}/app/assets/javascripts/*.{js, erb}").each { |f|
      js_files << File.basename(f)
    }

    config.assets.precompile += css_files.concat(js_files)
  end
end
