SinaiMount::Application.routes.draw do

  devise_for :users

  scope '/administration', module: 'administration', as: 'administration' do
    resources :countries, except: [:show, :destroy]
    resources :states, except: [:show, :destroy]
    resources :cities, except: [:show, :destroy]
    resources :jobs, except: [:destroy]
    resources :churches, except: [:destroy]
    resources :shepherds, except: [:destroy]
    resources :congregateds, except: [:destroy]
    resources :posts, except: [:destroy]
    resources :members, except: [:destroy] do
      get :naturalness, on: :collection
    end
  end

  get :cities_of_state, to: 'application#cities_of_state'

  get :show_address, to: 'application#show_address'

  root to: 'home#index'

  get '*a', to: 'application#render_error'
end
