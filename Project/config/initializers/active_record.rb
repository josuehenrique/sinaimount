ActiveSupport.on_load(:active_record) do
  include ActiveRecord::ListAttributes
  include ActiveRecord::SearchAttributes
  include ActiveRecord::ModalAttributes
  include ActiveRecord::Orderize
  include ActiveRecord::DeleteRestrict
  include ActiveRecord::Filterize

  extend EnumerateIt
end

module ActiveRecord
  class Base
    def to_s
      self.respond_to?(:nome) ? self.nome : super
    end
  end
end
