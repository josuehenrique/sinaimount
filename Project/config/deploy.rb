require 'bundler/capistrano'

set :application, "SinaiMount"
set :scm, :none
set :repository, "."
set :deploy_via, :copy
set :branch, "master"
set :ssh_options, { forward_agent: true }
ssh_options[:keys] = [ENV["CAP_PRIVATE_KEY"]] if ENV["CAP_PRIVATE_KEY"]
set :stage, :production
set :user, "bruno"
set :use_sudo, false
set :deploy_to, "/usr/local/home/bruno/#{application}"
set :domain, "demonstracao.SinaiMount.com.br"
set :keep_releases, 2
set :default_environment, {
  'PATH' => "/usr/local/rvm/gems/ruby-2.1.0/bin:/usr/local/rvm/gems/ruby-2.1.0@SinaiMount/bin:/usr/local/rvm/rubies/ruby-2.1.0/bin:/usr/local/rvm/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games",
  'RUBY_VERSION' => '2.1.0p0',
  'GEM_HOME' => '/usr/local/rvm/gems/ruby-2.1.0',
  'GEM_PATH' => '/usr/local/rvm/gems/ruby-2.1.0:/usr/local/rvm/gems/ruby-2.1.0@SinaiMount'
}

role :app, domain
role :web, domain
role :db, domain, primary: true

set :assets_role, [:web, :app]
load 'deploy/assets'

before  "deploy:assets:precompile", "bundle:install", "deploy:link_db"
after  "deploy:update_code", "deploy:migrate"

namespace :deploy do
  desc "Link in the production database.yml and assets"
  task :link_db do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end
end

namespace :passenger do
  desc "Restart Application"
  task :restart do
    run "touch #{current_path}/tmp/restart.txt"
  end
end

namespace :log do
  desc "A pinch of tail"
  task :tailf, roles: :app do
    run "tail -n 10000 -f #{shared_path}/log/#{rails_env}.log" do |channel, stream, data|
      puts "#{data}"
      break if stream == :err
    end
  end
end
